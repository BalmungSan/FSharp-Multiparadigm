﻿//Functional paradigm example (using lightweight syntax)
module Functional

//Binding values
let a = 5 + 2
//let a = a + 1 //This line fails because let bindings are immutable in F#

//Normal function
let add x y =
    x + y //The last line of a function is the return value

//Type Annotations 
let concat (s1:string) (s2:string) :string = //The operator ':' determine the type of a value
    s1 + s2

//Recursive functions
let rec fact n:int =     //The 'rec' keyword tells the compiler that this function is recursive
    match n with         //The 'match ... with' key words are used to discriminate the cases of a function
    | 0 | 1 -> 1         //The operator '|' match the parameter with the case
    | _ -> n * fact(n-1) //The operator '->' assigns the return value of a case of a function

//Lambda Functions
let double = (fun x -> x*2) //The 'fun' key word is used to declare a lambda function

//High order functions
let aply (foo:int -> int) value:int = //A high order function is a function that receives other function as a parameter
    foo value //Return the returned value of calling foo with value
let test = aply (fun x -> x+1) 5

//Lists
let list1 = [1; 2; 3]     //Lightweight list creation syntax
let list2 = [1..5]        //.. list creation syntax
let list3 = 1::[]         //Head::Tail list creatin syntax
let list4 = list1 @ list2 //Concat list
let array = [|1..5|]      //Array
let seque = {1..5}        //Sequence

//Forward pipe operator
let dificult = List.sum (List.map (fun x -> x * 2) (List.filter (fun x -> x % 2 = 0) [0..100]))
    //This line adds the double of the first 50 even numbers (powerful but difficult to understand)
let easy =
    [0..100]                            //The list with the numbers from 0 to 100
    |> List.filter (fun x -> x % 2 = 0) //The list of the even numbers of the above list
    |> List.map (fun x -> x * 2)        //The list of the double of the above list
    |> List.sum                         //The sum of the elements of the above list (return value)

//Discriminated union
type Shape =
| Circle of float              //The value here is the radius
| Square of double             //The value here is the side length
| Rectangle of double * double //The values here are the height and width
| Triangle of double * double  //The values here is the height and the base

//Functions with discriminated unions
let area myShape =
    match myShape with
    | Circle radius -> 3.1416 * radius * radius
    | Square s -> s * s
    | Rectangle (h, w) -> h * w
    | Triangle (h, b) -> h * b / 2.0