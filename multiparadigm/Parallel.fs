﻿//Parallel paradigm example
module Parallel

//A simple prime number detector
let isPrime (n:int) =
   let bound = int (sqrt (float n))
   seq {2 .. bound} |> Seq.forall (fun x -> n % x <> 0)

//We are using async workflows
let primeAsync n =
    async {return (n, isPrime n)} //Return a tuple (number:int, isprime:bool)

//Return primes between m and n using multiple threads
let primes n m =
    seq {n .. m}                     //For every numbers between m and n
        |> Seq.map primeAsync        //Check if the number is prime or not
        |> Async.Parallel            //Run in parallel (threads)
        |> Async.RunSynchronously    //Wait the above computation finishes before continue
        |> Array.filter snd          //Take only the prime numbers (_,true)
        |> Array.map fst             //Take the numbers
        |> Array.iter (printfn "%d") //Print every number in a new line