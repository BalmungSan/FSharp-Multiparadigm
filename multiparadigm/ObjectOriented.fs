﻿//Object oriented paradigm example
module ObjectOriented

//Record type similar to structures in C++
type Book =             //The 'type' keyword is used to declare a new data type
  { Name: string;
    Author: string;     
    Rating: int option; //The 'option' keyword mean that this field could be optional (use None)
    Year: int}

//Create a new record type
let favoriteBook = {
    Name = "Duma Key";
    Author = "Stephen King";
    Rating = Some 5;
    Year = 2008;
}

//Access the data of a record type
printfn "My favorite book is %s written by %s in the year %d and has a rating of %A" favoriteBook.Name favoriteBook.Author favoriteBook.Year favoriteBook.Rating;

//Define an object type (class)
type User(name:string, id:int, gender:char) =
    member this.Name = name;
    member this.ID = id;
    member this.Gender = gender;

//Create a new object and access its values
let myuser = new User("BalmungSan", 1234, 'M');

//Access the fields of an object
printfn "User %s with id:%d and gender:%c" myuser.Name myuser.ID myuser.Gender;

//Define a more complex object
type Encryptor(message) =
    //Private attribute message
    let mutable Message = message

    //Private function
    let Move c =
        let upperZero = int 'A' - 1; //Zero of the captial letters
        let lowerZero = int 'a' - 1; //Zero of the lowercase letters

        if System.Char.IsLetter(c) then
            if System.Char.IsUpper(c) then
                char (((int c + 13 - upperZero) % 26) + upperZero);
            else
                char (((int c + 13 - lowerZero) % 26) + lowerZero);
        else c
    
    //Get and set for message
    member this.MyMessage with get() = Message and set(newMessage) = Message <- newMessage

    //Public function
    member this.cesarEncryption() = 
        printfn "%s" <| (String.map Move this.MyMessage)

//Inheritance
[<AbstractClass>]
type Vehicle(id:string) = //Abstract parent class
    member this.ID = id;   
    
    //Abstract property
    abstract member Aceleration : int
    
    //Virtual property
    abstract member Velocity : int
    default this.Velocity = 60

    //Abstract method
    abstract member RunAcelerate : int -> int

    //Virtual method
    abstract member Run : int -> int
    default this.Run time = this.Velocity * time;

type Car(id:string, aceleration:int) = //Child class
   inherit Vehicle(id);                //Inherited from the father

   //Override abstract property
   override this.Aceleration = aceleration;

   //Override abstract method
   override this.RunAcelerate (time:int) = (this.Aceleration*time/2) + this.Velocity*time;

//Strings are objects in F# and come from the .Net
let toHackerTalk (phrase:string) =
    phrase.ToLower().Replace('t', '7').Replace('o', '0').Replace('i','1')

//Exceptions handling (.Net exceptions)
let divide x y =
  try
    Some(x/y) //Throws .net divide by zero exception
  with
    | :? System.DivideByZeroException as ex -> printfn "Exception! %s " (ex.Message); None

//Exception raising (F# exceptions)
exception Error1 of string
exception Error2 of string * int

let errors x y =
   try
      if x = y then raise (Error1("x"))
      else raise (Error2("x", 10))
   with
      | Error1(str) -> printfn "Error1 %s" str
      | Error2(str, i) -> printfn "Error2 %s %d" str i