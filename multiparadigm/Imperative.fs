﻿//Imperative paradigm example (using verbose syntax)
module Imperative

//Redefinition of variables
let mutable var = 5 //The 'mutable' keyword is used for defining variables
var <- var + 1;       //The '<-' operator is used to asing a new value to a variable

//Print to console
printfn "var is %d" var

//Ask a user input
System.Console.WriteLine("Enter a text");
let input = System.Console.ReadLine();
printfn "Your text is %s" input

//Parse a value
let (dsuccess,dvalue) = System.DateTime.TryParse("19/11/2015");
printf "Does the parse succes %b with value %A" dsuccess dvalue;

//If then esle
let parserInt (num:string) :int =
    let (isuccess,ivalue) = System.Int32.TryParse(num) in
    if isuccess then ivalue else -1

//If nested
let compare (x:int) (y:int) :string =
    if x = y then "Equals"
    elif x > y then "Greater"
    else "Lower"

//While
let lookForValue value maxValue =
    let mutable continueLooping = true;
    let randomNumberGenerator = new System.Random();
    
    //While the value is not found 
    while continueLooping do
        //Generate a random number between 1 and maxValue
        let rand = randomNumberGenerator.Next(maxValue);
        
        //If the random number is the value
        if rand = value then 
            //print a message saying the number was found and end the loop
            printfn "\nFound a %d!" value;
            continueLooping <- false;
        else  
            //If not print the number and continue
            printf "%d " rand;
    done

 //For ... to
 let count10() =
    for i = 0 to 10 do
        printf "%d " i;
    done
    printfn ""

//For .. downto
let reverse (maxValue:int) (minValue:int) :seq<int>=
    let mutable nums = Seq.empty
    for i = maxValue downto minValue do
            nums <- Seq.append nums [i];
    done
    nums;

//For .. in
let square = [for i in 1 .. 10 -> (i, i*i)]
for (a, asqr) in square do
    printfn "%d squared is %d" a asqr;
done