FSharp-Multiparadigm
F# Multiparadigm examples

Author: Luis Miguel Mejía Suárez (BalmungSan)

This is a visual studio F# project
Here are presented examples of the different paradigms that could be programmed in F#

Paradigms:
•	Concurrent
•	Dataflow
•	Declarative
•	Functional
•	Generic
•	Imperative
•	Reflection
•	Object oriented
•	Reactive
